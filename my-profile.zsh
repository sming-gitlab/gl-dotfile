
# ===================
# DIRECTORY
# ===================

alias ..='cd ../' # Go back 1 directory level
alias f='open -a Finder ./' # f:  Opens current directory in MacOS Finder
alias vs='code .'

# ===================
# Development
# ===================

# Run in .../gitlab
alias bi='bundle install'
alias yi='yarn install'
alias bm='bin/rails db:migrate RAILS_ENV=development'

alias bmt='bin/rails db:migrate RAILS_ENV=test'
alias bc='bin/rails c'

alias changelog='bin/changelog'
alias migrate='bin/rails db:migrate RAILS_ENV=development'
alias restart='bundle install && yarn install && bin/rails db:migrate RAILS_ENV=development'
alias start='bundle install && yarn install'
alias nuke='bundle exec rake dev:setup RAILS_ENV=development'
alias seed='bundle exec rake db:create db:migrate db:seed_fu'

## BREW ##
alias updatebrew="brew update && brew upgrade && brew cask upgrade"

# ===================
# GitLab
# ===================

alias translate='bin/rake gettext:regenerate'
alias tlog='tail -f log/development.log'

## DIRECTORY ##
alias gitlab='cd ~/repo/projects/'
alias kit='cd ~/repo/projects/gitlab-development-kit/'
alias ce='cd ~/repo/projects/gitlab-development-kit/gitlab/'
alias ui='cd ~/repo/gitlab-ui/'

# alias hq='cd ~/repo/projects/gitlab-development-kit/gdk-hq/'
# alias ee='cd ~/repo/projects/gitlab-development-kit/gdk-dev-ee/'
# alias ce='cd ~/repo/projects/gitlab-development-kit/gdk-ce/'
# alias ghq='cd ~/repo/projects/gitlab-development-kit/gdk-hq/gitlab/'
# alias gee='cd ~/repo/projects/gitlab-development-kit/gdk-dev-ee/gitlab/'
# alias gce='cd ~/repo/projects/gitlab-development-kit/gdk-org/gitlab'

## GDK (Run in gdk-ce | gdk-ee) ##
alias one='gdk start'
alias two='gdk tail'
alias three='gdk start postgresql redis webpack'
alias gdks='gdk status'
alias gdkr='gdk restart'
alias gdkrw='gdk restart webpack'
alias noo='gdk stop'
# alias one='gdk start postgresql redis'
# alias one='gdk run db'
# alias two='gdk run app'

## GL: Jasmine -> Jest ##
function jest1() {
  cp -r $1 $2
  rm -r $1
}

function jest2() {
  cd ~/repo/jestodus-codemod/
  jscodeshift --parser=babylon -t src/index.js ../projects/gitlab-development-kit/gitlab/$1
}

# ===================
# Test & Lint
# ===================

alias lint-all='yarn run eslint && yarn run stylelint && yarn prettier-all'

alias lint='yarn run eslint'
alias slint='yarn run stylelint'
alias plint='yarn prettier-all'
alias tlint='rake gettext:lint' # lint translation

# Karma
alias kload='bundle exec rake karma:fixtures'
alias karma= 'yarn karma-start'
alias kf='yarn karma-start -f'

# Jest
alias jd='yarn jest-debug'
alias jest='yarn jest'
alias jw='yarn jest --watch'
alias jw-all='yarn jest --watchAll'

# Prettier - Run Prettier on files
alias pretty='yarn prettier-staged-save'
alias pretty-all='yarn prettier-all-save'

# Check which files need to be Prettier
alias pcheck='yarn prettier-staged'
alias pcheck-all='yarn prettier-all'

# RSpec
alias rchrome='CHROME_HEADLESS=0 bundle exec rspec'
alias rchromes='CHROME_HEADLESS=0 bundle exec spring rspec'
alias rspecs='bin/spring rspec'
alias rtest='bundle exec rspec'

# ===================
# GIT FUNCTION
# https://github.com/robbyrussell/oh-my-zsh/blob/master/plugins/git/git.plugin.zsh
# ===================

# The name of the current branch
function current_branch() {
  git_current_branch
}

# Pretty log messages
function _git_log_prettily(){
  if ! [ -z $1 ]; then
    git log --pretty=$1
  fi
}
compdef _git _git_log_prettily=git-log

# Warn if the current branch is a WIP
function work_in_progress() {
  if $(git log -n 1 2>/dev/null | grep -q -c "\-\-wip\-\-"); then
    echo "WIP!!"
  fi
}

# ===================
# GIT ALIAS
# ===================

alias ga='git add -v'
alias gb='git --no-pager branch'
# alias gb='git branch'
alias rename='git branch -m'

alias ggsup='git branch --set-upstream-to=origin/$(git_current_branch)'
alias gpsup='git push --set-upstream origin $(git_current_branch)'

alias gd='git diff'
alias gdca='git diff --cached'
alias gdcw='git diff --cached --word-diff'
alias gdct='git describe --tags $(git rev-list --tags --max-count=1)'
alias gds='git diff --staged'
alias gdt='git diff-tree --no-commit-id --name-only -r'
alias gdw='git diff --word-diff'

alias gs='git status'
alias gc='git commit'
alias gm='git commit -m'
alias gca='git commit --amend'
alias gce='git commit --amend --no-edit'
alias gcco='git commit -c ORIG_HEAD'
alias gso='git show'

alias remote='git remote -v'

## Push ##
alias up='git push origin "$(git_current_branch)"'
alias upf='git push --force-with-lease origin "$(git_current_branch)"'
alias upnew='git push --set-upstream origin $(git_current_branch)'

## Pull ##
alias down='git pull origin "$(git_current_branch)"'

## Fetch ##
alias gf='git fetch'
alias gfo='git fetch origin'
alias gfm='git fetch origin/master'
alias gfb='git fetch origin "$(git_current_branch)"'

## Checkout ##
alias go='git checkout'
alias gom='git checkout master'

## Cherry Pick ##
alias gcp='git cherry-pick'
alias gcpc='git cherry-pick --continue'
alias gcpa='git cherry-pick --abort'

alias cherry='git cherry-pick'
alias cherryc='git cherry-pick --continue'
alias cherrya='git cherry-pick --abort'

## Stash ##
alias gt'git stash'
alias gta='git stash apply'
alias gtu='git stash --include-untracked'
alias gsa='git stash apply'

alias stash'git stash'
alias stasha='git stash apply'
alias stashall='git stash --include-untracked'

## Log ##
alias gl="git log --pretty=format:'%C(yellow)%h %Cred%ad %Cblue%an%Cgreen%d %Creset%s' --date=short"
alias log='git log'
alias glo='git log --oneline'
alias glg='git log --graph'
alias glog='git log --oneline --decorate --graph'
alias gls='git log --stat'
alias glsp='git log --stat -p'

## Delete ##
alias die='git branch -d'
alias DIE='git branch -D'
alias dieup='git push origin --delete'

## Reset ##
alias undo='git reset'
alias UNDO='git reset --soft HEAD~1'
alias FUCK='git reset --hard HEAD'
alias RESET='git reset --hard origin/$(git_current_branch)'
alias RESETM='git reset --hard origin/master'

## Rebase ##
alias gr='git rebase'
alias grm='git rebase origin/master'
alias grmi='git rebase -i origin/master'
alias grc='git rebase --continue'

function head() {
  git rebase -i HEAD~$1
}

function ONTO() {
  git rebase --onto=master HEAD~$1
}

## FIXUP ##
function fix() {
  git commit --fixup=$1
}

function fix^() {
  git rebase --interactive --autosquash $1^
}

## Patch ##
function patch() {
  git apply < /Users/samanthaming/repo/$1.patch
}

function createPatch() {
  git diff > /Users/samanthaming/repo/$1.patch
}

## Delete Branch
function deleteb(){
  git branch | grep "$1" | xargs git branch -D
}
